use crate::{g, y0, Ball}; //import classes/consts. No need for mod - that is only in main.rs
use plotly::common::Mode; //import plotting stuff
use plotly::{Plot, Scatter};

pub fn do_and_plot(time_step: f32, b: Ball) { //function to make a graph given ball type/time step
    let mut vel_vec = Vec::new(); //make empty lists for traces
    let mut pos_vec = Vec::new(); //mut means they can be changed after declaration
    let mut t_vec = Vec::new();
    let mut y: f32 = y0; //set current height to fall from

    let mut t: f32 = 0.0; //set 'i' variable - rust doesn't allow for loops with custom increments
    while y >= 0.0 { //whilst we haven't yet hit the ground.
        y -= get_pos(t, b); //fall a bit, given how far to fall
        pos_vec.push(y); //add the new height to the trace list
        vel_vec.push(get_vel(t, b)); //add the new velocity to the trace list
        t_vec.push(t); //add time to the time trace

        t += time_step; //increment current time
    }

    let mut plot = Plot::new(); //make a new graph
    let vel_trace = Scatter::new(t_vec.clone(), vel_vec) //make a trace with the velocity as x, and the time as y
        .name("Velocity") //set the name as "Velocity"
        .mode(Mode::LinesMarkers); //with both crosses and lines

    let pos_trace = Scatter::new(t_vec, pos_vec) //same for the y positions
        .name("Y Position")
        .mode(Mode::LinesMarkers);

    plot.add_trace(vel_trace); //add the traces to the graph
    plot.add_trace(pos_trace);
    plot.to_html(String::from(b.name) + ".html"); //&strs cannot be concatenated, but one String and one &str can be. "hello" is a primitive &str, and String::from("hello") is a String
    plot.show(); //open the plot in the web browser
}

fn get_vel(t: f32, b: Ball) -> f32 { //get the velocity given the time, and the ball type
    //ex 2
    let first_top = 2.0 * b.mass * g; //get the top half of the first fraction
    let first_sqrt = (first_top / get_first_btm(b)).sqrt(); //get the first fraction

    let scnd_sqrt = get_scnd_thing(b).sqrt() * t; //get the second half

    first_sqrt * scnd_sqrt.tanh() //return the new velocity
}

fn get_pos(t: f32, b: Ball) -> f32 {
    //ex 3
    let first_top = 2.0 * b.mass; //get the top half of the first fraction

    let first_thing = first_top / get_first_btm(b); //get the first fraction
    let scnd_thing = get_scnd_thing(b) * t; //get the second fraction

    first_thing * (scnd_thing * t).cosh().ln() //return the new height
}

fn area_density (b: Ball) -> f32 { //More Common Stuff
    b.density * b.area
}

fn get_first_btm(b: Ball) -> f32 { //I noticed a common bottom half of fractions, so I exported it to a new method
    b.diameter * area_density(b)
}

fn get_scnd_thing(b: Ball) -> f32 { //Same as get_first_btm, but for a whole fraction
    let top = b.drag * area_density(b) * g;
    let btm = 2.0 * b.mass;

    top / btm
}
