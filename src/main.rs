mod ball;
mod exercises; //import ball.rs, exercises.rs

use ball::Ball; //use Ball from ball.rs....
use exercises::do_and_plot;

#[allow(non_upper_case_globals)] //consts are supposed to be ALL_CAPS, but if I add this line, the compiler doesn't shout at me
pub const g: f32 = 9.81; //gravity. const means it cannot be changed
#[allow(non_upper_case_globals)]
pub const y0: f32 = 5_000.0; //y starting pos. In rust, to aid readability of large numbers, you can add underscores, which are ignored by the compiler.
//I used 5000 rather than 440 to make sure it was plateauing properly at the terminal velocity.

fn main() {
    let step: f32 = 0.01; //timestep variable
    let types = vec![Ball::bowling_ball(), Ball::basketball(), Ball::baseball()]; //declare a list with each type of ball

    for b in types { //foreach loop
        do_and_plot(step, b);
    }
}
