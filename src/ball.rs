//Copy/Clone allow it to be moved around, and automatically copied when need be
//debug allows it to be printed out for debugging.
#[derive(Copy, Clone, Debug)]
pub struct Ball { //declare ball
    pub mass: f32, //pub means public - can be accessed without getters/setters; mass is the name; f32 is the type - a 32-bit floating point integer
    pub drag: f32,
    pub diameter: f32,
    pub radius: f32,
    pub area: f32,
    pub volume: f32,
    pub density: f32,
    pub name: &'static str, //&str means a reference to a string. Because we have a reference, we don't know how long it should stay for, so we use 'static to keep it alive.
}

impl Ball {
    fn new(mass: f32, drag: f32, diameter: f32, name: &'static str) -> Self { //constructor with args. no pub means that it can only be 'made' from inside this class, unless helper methods are used.
        let radius = diameter / 2.0;
        let area = std::f32::consts::PI * radius * radius;
        let volume = area * (4.0 / 3.0) * radius;
        let density = mass / volume; //declare variables for actual constructor

        Ball { //actual constructor
            mass, //instead of typing 'mass: mass', you can just type mass
            drag,
            diameter,
            radius,
            area,
            volume,
            density,
            name,
        } //in rust, you don't need to 'return' anything - just don't use a semicolon
    }

    #[allow(dead_code)] //make sure the compiler doesn't warn me if one of these code blocks isn't used
    pub fn bowling_ball() -> Self { //methods to make ball types
        //21lbs, spherical, 22cm
        Ball::new(8.618, 0.47, 0.22, "Bowling-Ball")
    }
    pub fn basketball() -> Self {
        //20-22oz (21), spherical, 9.43- 9.51 in rad" (9.47)
        Ball::new(0.595, 0.47, 0.24 * 2.0, "Basketball")
    }
    pub fn baseball() -> Self {
        //142-149g (145.5), spherical, 73-76mm in diameter (74.5)
        Ball::new(14.5, 0.47, 0.745, "Baseball")
    }
}
